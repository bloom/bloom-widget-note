#include "../config.h"

#include "bloom-widget-note.h"
#include "bloom-widget-dbus-adaptor.h"
#include "bloom-widget-qt.h"

#define BLOOM_WIDGET_NOTE_SERVICE "org.agorabox.Bloom.Widget.Note"

int main(int argc, char **argv) {
    QString object_path;
    QString service_name;
    QDBusConnection connection = QDBusConnection::sessionBus();

    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    QApplication app(argc, argv);
 
    BloomWidgetNote *widget = new BloomWidgetNote();
    new WidgetAdaptor(widget);

    QString service = get_free_service_name(BLOOM_WIDGET_NOTE_SERVICE);
    connection.registerService(service);
    connection.registerObject("/Widget", widget);

    return app.exec();
}

