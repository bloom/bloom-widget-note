
#ifndef __BLOOM_WIDGET_NOTE_H__
#define __BLOOM_WIDGET_NOTE_H__

#include <QFile>
#include <QTextStream>
#include <QIODevice>

#include <QtCore>
#include <QtGui>
#include <QUrl>
#include <QtDBus/QtDBus>
#include <QX11EmbedWidget>

#include "gconfitem.h"

#include <glib/gi18n.h>

class GnoteRemoteControl;

class BloomWidgetNote : public QX11EmbedWidget
{
    Q_OBJECT

    public:
        BloomWidgetNote();
        ~BloomWidgetNote();

    public Q_SLOTS:
        int create(int container, const QString &gconf);

    public slots:
        void    addNote                 ();
        void    deleteNoteA             ();
        void    prevNote                ();
        void    nextNote                ();
        void    changeCharFormat        (QTextCharFormat);
        void    set_FontBold            (bool active);
        void    set_FontStrike          (bool active);
        void    set_FontSize            (QAction *);
        void    activate_save           ();
        void    activate_save           (QString);
        void    auto_save               ();

    protected:
        void    resizeEvent             (QResizeEvent*);
        
        void    getAllNotes             ();
        void    generate_start_note     ();
        QString add_start_note          ();
        void    displayNote             (int index);
        void    saveNote                ();
        bool    saveCompleteXMLNote     ();
        void    deleteAllNotes          ();
        void    save_uri                (QString newUri);
        void    delete_uri              (QString uri);
        
        GnoteRemoteControl *remote_control;
        QString         uri_filename;
        QLineEdit*      change_note_title;
        QString         validUri;
        QString         validTitle;
        QTimer*         save_timer;
        
        QHBoxLayout* topLayout;
        QHBoxLayout* bodyLayout;
        QHBoxLayout* bottomLayout;
        QHBoxLayout* titleLayout;
        QHBoxLayout* tailLayout;
        QVBoxLayout* pageLayout;
        QVBoxLayout* contentLayout;
        QVBoxLayout* main_layout;
        
        QAction* setItalicButton;
        QAction* setBoldButton;
        QAction* setStrikeButton;
        QAction* smallSizeButton;
        QAction* normalSizeButton;
        QAction* largeSizeButton;
        QAction* hugeSizeButton;
        
        QLabel*       posInNotes;
        int           notesCount;
        int           currentNote;
        QLineEdit*    lineTitle;
        QTextEdit*    noteText;
        QStringList   noteUris;
        QStringList   noteTitles;
        QStringList   noteContents;
        bool          save_needed;

        GConfItem     *notes;
};

#endif //__BLOOM_WIDGET_NOTE_H__
