/*
 * bloom-widget-note 
 * 
 * Copyright (C) 2010, Agorabox.
 *
 * Author: CLEMENT Julien
 * 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bloom-widget-note.h"
#include "gnote-remote-control-proxy.h"

#include <QtDBus/QtDBus>
#include <sstream>

#define FONTSMALL   7
#define FONTNORMAL  10
#define FONTLARGE   14 
#define FONTHUGE    18
#define FONTNAME    "Helvetica"

#define TOMBOY_DESTINATION "org.gnome.Gnote"
#define TOMBOY_OBJECT_PATH "/org/gnome/Gnote/RemoteControl"
#define TOMBOY_DBUS_INTERFACE "org.gnome.Gnote.RemoteControl"

BloomWidgetNote::BloomWidgetNote() :
    QX11EmbedWidget(NULL)
{
    notes = NULL;
    remote_control = NULL;
    setWindowFlags(Qt::ToolTip);
}

BloomWidgetNote::~BloomWidgetNote()
{
    if (notes)
        delete notes;
    if (remote_control)
        delete remote_control;
}

int BloomWidgetNote::create(int container, const QString &gconf)
{
    notes = new GConfItem(gconf + "/note");

    remote_control = new GnoteRemoteControl(QString(TOMBOY_DESTINATION), QString(TOMBOY_OBJECT_PATH),
                                            QDBusConnection::sessionBus(), this);

    main_layout = new QVBoxLayout();
    main_layout->setContentsMargins(0,0,0,0);
    
    QString theme = "";
    QFile file(THEMEDIR "/bloom-widget-note.css");
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning("File can't be found or opened");
    } else {
        QTextStream stream(&file);
        while(!stream.atEnd()) {
            theme += stream.readLine();
        }
    }
    file.close();
    theme.replace("THEMEDIR", THEMEDIR);
    theme.prepend(styleSheet());
    setStyleSheet(theme);

    //setStyle(new QPlastiqueStyle);

    QHBoxLayout *topLayout = new QHBoxLayout();
    QHBoxLayout *bodyLayout = new QHBoxLayout();
    QHBoxLayout *bottomLayout = new QHBoxLayout();
    QVBoxLayout *pageLayout = new QVBoxLayout();
    
    topLayout->setObjectName("top_layout");
    topLayout->setContentsMargins(0,0,0,0);
    topLayout->setSpacing(0);
    bodyLayout->setObjectName("body_layout");
    bodyLayout->setContentsMargins(0,0,0,0);
    bodyLayout->setSpacing(0);
    bottomLayout->setObjectName("bottom_layout");
    pageLayout->setObjectName("page_layout");
    pageLayout->setContentsMargins(0,0,0,0);
    pageLayout->setSpacing(0);

    QLabel * top_background = new QLabel();
    top_background->setObjectName("top_background");
    top_background->setAutoFillBackground(true);
    top_background->setFixedHeight(3);
    top_background->setLayout(topLayout);

    QLabel * body_background = new QLabel();
    body_background->setObjectName("body_background");
    body_background->setAutoFillBackground(true);
    body_background->setLayout(bodyLayout);
    
    QLabel * top_left_img = new QLabel();
    top_left_img->setObjectName("top_left_img");
    top_left_img->setFixedSize(29, 3);
    top_left_img->setAutoFillBackground(true);

    QLabel * top_img = new QLabel(this);
    top_img->setObjectName("top_img");
    top_img->setFixedHeight(3);
    top_img->setAutoFillBackground(true);

    QLabel * top_right_img = new QLabel(this);
    top_right_img->setObjectName("top_right_img");
    top_right_img->setFixedSize(9, 3);
    top_right_img->setAutoFillBackground(true);

    QLabel * left_img = new QLabel(this);
    left_img->setObjectName("left_img");
    left_img->setFixedWidth(29);
    // left_img->setAutoFillBackground(true);

    QLabel * right_img = new QLabel(this);
    right_img->setObjectName("right_img");
    right_img->setFixedWidth(9);
    right_img->setAutoFillBackground(true);

    QLabel * bottom_left_img = new QLabel(this);
    bottom_left_img->setObjectName("bottom_left_img");
    bottom_left_img->setFixedSize(29,3);
    bottom_left_img->setAutoFillBackground(true);

    QLabel * bottom_img = new QLabel(this);
    bottom_img->setObjectName("bottom_img");
    bottom_img->setFixedHeight(3);
    bottom_img->setAutoFillBackground(true);

    QLabel * bottom_right_img = new QLabel(this);
    bottom_right_img->setObjectName("bottom_right_img");
    bottom_right_img->setFixedSize(9, 3);
    bottom_right_img->setAutoFillBackground(true);

    /* Note title and buttons */
    QHBoxLayout    *titleLayout = new QHBoxLayout();
    titleLayout->setObjectName("title_layout");
    lineTitle = new QLineEdit(QString::fromUtf8(_("Note title")));
    lineTitle->setObjectName("line_title");
    lineTitle->setAutoFillBackground(true);
    
    QPushButton *addNoteButton = new QPushButton();
    addNoteButton->setFixedSize(25, 25);
    addNoteButton->setFocusPolicy(Qt::NoFocus);
    addNoteButton->setObjectName("add_note_button");
    QPushButton *deleteNoteButton = new QPushButton();
    deleteNoteButton->setFocusPolicy(Qt::NoFocus);
    deleteNoteButton->setFixedSize(25, 25);
    deleteNoteButton->setObjectName("delete_note_button");
    QPushButton *fontButton = new QPushButton();
    fontButton->setFocusPolicy(Qt::NoFocus);
    fontButton->setFixedSize(24, 26);
    fontButton->setObjectName("font_button");
    fontButton->setStyle(new QPlastiqueStyle);
    titleLayout->addWidget(lineTitle);
    titleLayout->addWidget(addNoteButton);
    titleLayout->addWidget(deleteNoteButton);
    
    QMenu * font_menu = new QMenu();
    font_menu->setStyle(new QPlastiqueStyle);
    font_menu->setFixedWidth(120);
    setItalicButton = new QAction(this);
    setItalicButton->setCheckable(true);
    setItalicButton->setObjectName("set_italic_button");
    setItalicButton->setText(QString::fromUtf8(_("&Italic")));
    setItalicButton->setStatusTip(QString::fromUtf8(_("&Toggle italic style")));
    font_menu->addAction(setItalicButton);
    
    setBoldButton = new QAction(this);
    setBoldButton->setCheckable(true);
    setBoldButton->setObjectName("set_bold_button");
    setBoldButton->setText(QString::fromUtf8(_("&Bold")));
    setBoldButton->setStatusTip(QString::fromUtf8(_("&Toggle bold style")));
    font_menu->addAction(setBoldButton);
    
    setStrikeButton = new QAction(this);
    setStrikeButton->setCheckable(true);
    setStrikeButton->setObjectName("set_strike_button");
    setStrikeButton->setText(QString::fromUtf8(_("&Strike")));
    setStrikeButton->setStatusTip(QString::fromUtf8(_("&Toggle strike style")));
    font_menu->addAction(setStrikeButton);
    
    font_menu->addSeparator()->setText(QString::fromUtf8(_("Font Size")));
    
    QActionGroup * fontSizeGroup = new QActionGroup(this);
    smallSizeButton = new QAction(this);
    smallSizeButton->setCheckable(true);
    smallSizeButton->setObjectName("small_size_button");
    smallSizeButton->setText(QString::fromUtf8(_("&Small")));
    smallSizeButton->setStatusTip(QString::fromUtf8(_("&Set small size font")));
    fontSizeGroup->addAction(smallSizeButton);
    font_menu->addAction(smallSizeButton);
    
    normalSizeButton = new QAction(this);
    normalSizeButton->setCheckable(true);
    normalSizeButton->setObjectName("normal_size_button");
    normalSizeButton->setText(QString::fromUtf8(_("&Normal")));
    normalSizeButton->setStatusTip(QString::fromUtf8(_("&Set normal size font")));
    fontSizeGroup->addAction(normalSizeButton);
    font_menu->addAction(normalSizeButton);
    
    largeSizeButton = new QAction(this);
    largeSizeButton->setCheckable(true);
    largeSizeButton->setObjectName("large_size_button");
    largeSizeButton->setText(QString::fromUtf8(_("&Large")));
    largeSizeButton->setStatusTip(QString::fromUtf8(_("&Set large size font")));
    fontSizeGroup->addAction(largeSizeButton);
    font_menu->addAction(largeSizeButton);
    
    hugeSizeButton = new QAction(this);
    hugeSizeButton->setCheckable(true);
    hugeSizeButton->setObjectName("huge_size_button");
    hugeSizeButton->setText(QString::fromUtf8(_("&Huge")));
    hugeSizeButton->setStatusTip(QString::fromUtf8(_("&Set huge size font")));
    fontSizeGroup->addAction(hugeSizeButton);
    font_menu->addAction(hugeSizeButton);
    
    normalSizeButton->setChecked(true);
    
    connect(addNoteButton, SIGNAL(clicked()), this, SLOT(addNote()));
    connect(deleteNoteButton, SIGNAL(clicked()), this, SLOT(deleteNoteA()));
    
    /* Separator line */
    QFrame *separator = new QFrame();
    separator->setFrameStyle(QFrame::HLine);
    separator->setObjectName("separator_line");

    /* Note text */
    QFont myFont(FONTNAME, 10);
    QFont myTitleFont(FONTNAME, 12);
    lineTitle->setFont(myTitleFont);
    noteText = new QTextEdit();
    noteText->setFontPointSize(10);
    QTextCharFormat format = noteText->currentCharFormat();
    format.setFont(myFont);
    noteText->setCurrentCharFormat(format);
    noteText->setCurrentFont(myFont);
    noteText->setFont(myFont);
    noteText->setObjectName("noteText");
    noteText->setAcceptRichText(true);
    noteText->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    
    connect(setItalicButton, SIGNAL(triggered(bool)), noteText, SLOT(setFontItalic(bool)));
    connect(setBoldButton, SIGNAL(triggered(bool)), this, SLOT(set_FontBold(bool)));
    connect(setStrikeButton, SIGNAL(triggered(bool)), this, SLOT(set_FontStrike(bool)));
    connect(fontSizeGroup, SIGNAL(triggered(QAction *)), this, SLOT(set_FontSize(QAction *)));
    connect(noteText, SIGNAL(currentCharFormatChanged(QTextCharFormat)), this, SLOT(changeCharFormat(QTextCharFormat)));
    
    connect(noteText, SIGNAL(textChanged()), this, SLOT(activate_save()));
    connect(lineTitle, SIGNAL(textChanged(QString)), this, SLOT(activate_save(QString)));
    fontButton->setMenu(font_menu);
    titleLayout->addWidget(fontButton);
    
    /* Notes navigation */
    QHBoxLayout    *tailLayout = new QHBoxLayout();
    tailLayout->setObjectName("tail_layout");
    QPushButton *prevNoteButton = new QPushButton();
    prevNoteButton->setObjectName("prev_note");
    prevNoteButton->setFixedSize(25, 18);
    QPushButton *nextNoteButton = new QPushButton();
    nextNoteButton->setObjectName("next_note");
    nextNoteButton->setFixedSize(25, 18);
    nextNoteButton->setFocusPolicy(Qt::NoFocus);
    prevNoteButton->setFocusPolicy(Qt::NoFocus);

    posInNotes = new QLabel();
    posInNotes->setFixedWidth(40);
    posInNotes->setObjectName("pos_in_notes_label");
    posInNotes->setAutoFillBackground(true);
    posInNotes->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    
    tailLayout->addWidget(prevNoteButton);
    tailLayout->addWidget(posInNotes);
    tailLayout->addWidget(nextNoteButton);
    
    tailLayout->setAlignment(prevNoteButton, Qt::AlignRight);
    tailLayout->setAlignment(nextNoteButton, Qt::AlignLeft);

    connect(prevNoteButton, SIGNAL(clicked()), this, SLOT(prevNote()));
    connect(nextNoteButton, SIGNAL(clicked()), this, SLOT(nextNote()));
    
    QVBoxLayout *contentLayout = new QVBoxLayout();
    contentLayout->addLayout(titleLayout);
    contentLayout->addWidget(separator);
    contentLayout->addWidget(noteText);
    contentLayout->addLayout(tailLayout);

    topLayout->setDirection(QBoxLayout::LeftToRight);
    topLayout->addWidget(top_left_img);
    topLayout->addWidget(top_img);
    topLayout->addWidget(top_right_img);

    bodyLayout->setDirection(QBoxLayout::LeftToRight);
    bodyLayout->addWidget(left_img);
    bodyLayout->addItem(new QSpacerItem(5,0));
    bodyLayout->addLayout(contentLayout);
    bodyLayout->addItem(new QSpacerItem(5,0));
    bodyLayout->addWidget(right_img);
    
    bottomLayout->setDirection(QBoxLayout::LeftToRight);
    bottomLayout->addWidget(bottom_left_img);
    bottomLayout->addWidget(bottom_img);
    bottomLayout->addWidget(bottom_right_img);

    pageLayout->addWidget(top_background);
    //pageLayout->addLayout(topLayout);
    pageLayout->addWidget(body_background);
    //pageLayout->addLayout(bodyLayout);
    pageLayout->addLayout(bottomLayout);

    main_layout->addLayout(pageLayout);

    notesCount = 0;
    getAllNotes();
    currentNote = 0;
    if(notesCount == 0) {
        addNote();
    }
    
    displayNote(0);

    /* Auto save note every 30sec */
    save_timer = new QTimer(this);
    connect(save_timer, SIGNAL(timeout()), this, SLOT(auto_save()));
    save_timer->start(30000);

    setLayout(main_layout);
    embedInto(container);
    show();

    return 0;
}

void BloomWidgetNote::displayNote(int index)
{
    if(index < notesCount) {
        lineTitle->setText(noteTitles.at(index));
        lineTitle->setCursorPosition(0);
        QString pageCount = QString::number(notesCount);
        QString pagePos = QString::number(index+1);
        pagePos += "/";
        pagePos += pageCount;
        posInNotes->setText(pagePos);
        noteText->setHtml(noteContents.at(index));
        save_needed = false;
    } else {
        lineTitle->setText(QString::fromUtf8(_("Enter note title here")));
        QString pageCount = QString::number(notesCount);
        QString pagePos = QString::number(1);
        pagePos += "/";
        pagePos += pageCount;
        posInNotes->setText(pagePos);
        noteText->setText("");
    }
}

void BloomWidgetNote::getAllNotes()
{
    QStringList uriList = notes->value().toStringList();
    notesCount = uriList.size();
    
    for (int i = 0 ; i < uriList.size() ; i++) {
        if (!uriList.at(i).isEmpty()) {
            QString currentURI(uriList.at(i));
            noteUris.append(currentURI);
            QString noteTitle = remote_control->GetNoteTitle(currentURI);
            noteTitles.append(noteTitle);
            QString noteContent = remote_control->GetNoteContents(currentURI);
            noteContents.append(noteContent.section('\n', 1));
        }
    }
}

void BloomWidgetNote::addNote()
{
    if(notesCount > 0) {
        if(!saveCompleteXMLNote()) {
            return;
        }
    }
    notesCount++;
    currentNote = notesCount-1;

    QString noteUri;
    /*if(currentNote == 0) {
        noteUri = add_start_note();
    } else {
       noteUri = remote_control->CreateNote();
    }*/
    noteUri = remote_control->CreateNote();
    if(!noteUri.isEmpty()) {
        noteUris.append(noteUri);
        /*if(currentNote == 0) {
            generate_start_note();
            
        } else {*/
            noteTitles.append(remote_control->GetNoteTitle(noteUri));
            noteContents.append(QString::fromUtf8(_("Describe your note content here.")));
        //}
        save_uri(noteUri);
        displayNote(currentNote);
        save_needed = true;
        saveCompleteXMLNote();
    }
}

void BloomWidgetNote::generate_start_note()
{
    noteTitles.append(QString::fromUtf8(_("Welcome to note widget")));
    QString span_italic = "<span style='font-style:italic;'>";
    QString span_bold = "<span style='font-weight:600;'>";
    QString span_striked = "<span style='text-decoration: line-through;'>";
    QString span_small = "<span style='font-size:7pt;'>";
    QString span_big = "<span style='font-size:14pt;'>";
    QString span_huge = "<span style='font-size:18pt;'>";
    QString end_span = "</span>";

    QString content = "";
    content.append(QString::fromUtf8(_("Here you can write new notes using font styles ")));
    content.append(span_italic + QString::fromUtf8(_("italic")) + end_span);
    content.append(", " + span_bold + QString::fromUtf8(_("bold")) + end_span);
    content.append(" " + QString::fromUtf8(_("and")) + " " + span_striked + QString::fromUtf8(_("crossed out")) + end_span + "." + "<br><br>");

    content.append(QString::fromUtf8(_("You can also choose font size among ")));
    content.append(span_small + QString::fromUtf8(_("small")) + end_span + ", " + QString::fromUtf8(_("normal")) + ", ");
    content.append(span_big + QString::fromUtf8(_("big")) + end_span + " " + QString::fromUtf8(_("or")) + " ");
    content.append(span_huge + QString::fromUtf8(_("huge")) + end_span + ". <br><br>");

    content.append(QString::fromUtf8(_("Notes management allows you to add new notes, delete existing ones and browse all of them.")));

    noteContents.append(content);
}

QString BloomWidgetNote::add_start_note()
{
    QString start_note_uri = remote_control->FindNote(QString::fromUtf8(_("Welcome to note widget")));
    qDebug() << "DEBUG: start note uri : " << start_note_uri;
    if(start_note_uri == "") {
        start_note_uri = remote_control->CreateNote();
        qDebug() << "DEBUG: NEW start note uri : " << start_note_uri;
    }
    return start_note_uri;
}

void BloomWidgetNote::deleteNoteA()
{
    remote_control->DeleteNote(noteUris.at(currentNote));
    delete_uri(noteUris.at(currentNote));
    noteUris.removeAt(currentNote);
    noteTitles.removeAt(currentNote);
    noteContents.removeAt(currentNote);
    if(currentNote > 0)
        currentNote--;
    if(notesCount > 0) {
        notesCount--;
        if(notesCount == 0) {
            addNote();
        }
    }
    displayNote(currentNote);
}

void BloomWidgetNote::deleteAllNotes()
{
    for(int i=0 ; i<noteUris.size() ; i++) {
        remote_control->DeleteNote(noteUris.at(i));
    }
    noteUris.clear();
    noteTitles.clear();
    noteContents.clear();
}

void BloomWidgetNote::saveNote()
{    
    if(!save_needed)
        return;

    noteContents.replace(currentNote, noteText->toHtml());
    noteTitles.replace(currentNote, lineTitle->text());
    QString data(noteTitles.at(currentNote));
    data.append("\n\n");
    QString tomboyNoteContent = noteContents.at(currentNote);
    data.append(tomboyNoteContent);
    
    remote_control->SetNoteContents(noteUris.at(currentNote), data);

    save_needed = false;
}

bool BloomWidgetNote::saveCompleteXMLNote()
{
    if(!save_needed)
        return true;
    
    noteContents.replace(currentNote, noteText->toHtml());
    noteTitles.replace(currentNote, lineTitle->text());

    if(noteTitles.at(currentNote).isEmpty()) {
        QDialog *change_Title = new QDialog(this, Qt::Popup);
        QFrame *frame = new QFrame(change_Title);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setGeometry(0, 0, 250, 90);

        QPushButton *bouton1 = new QPushButton("Ok");

        QVBoxLayout *vbox = new QVBoxLayout;
        QHBoxLayout *hbox = new QHBoxLayout;
        vbox->addWidget(new QLabel(QString::fromUtf8(_("Please enter a note title"))));
        hbox->addItem(new QSpacerItem(125,0));
        hbox->addWidget(bouton1);
        hbox->addItem(new QSpacerItem(125,0));
        vbox->addLayout(hbox);
        frame->setLayout(vbox);
        connect(bouton1, SIGNAL(clicked()), change_Title, SLOT(accept()));
        connect(change_Title, SIGNAL(accepted()), lineTitle, SLOT(setFocus()));

        change_Title->exec();
        save_timer->start(30000);
        return false;
    }

    QString existingUri = remote_control->FindNote(noteTitles.at(currentNote));
    if(!existingUri.isEmpty() && existingUri.compare(noteUris.at(currentNote)) != 0) {
        QDialog *change_Title = new QDialog(this, Qt::Popup);
        QFrame *frame = new QFrame(change_Title);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setGeometry(0, 0, 250, 90);

        QPushButton *bouton1 = new QPushButton("Ok");

        QVBoxLayout *vbox = new QVBoxLayout;
        QHBoxLayout *hbox = new QHBoxLayout;
        vbox->addWidget(new QLabel(_("Note title already exists, please modify it")));
        hbox->addItem(new QSpacerItem(125,0));
        hbox->addWidget(bouton1);
        hbox->addItem(new QSpacerItem(125,0));
        vbox->addLayout(hbox);
        frame->setLayout(vbox);
        connect(bouton1, SIGNAL(clicked()), change_Title, SLOT(accept()));
        connect(change_Title, SIGNAL(accepted()), lineTitle, SLOT(selectAll()));
        connect(change_Title, SIGNAL(accepted()), lineTitle, SLOT(setFocus()));

        change_Title->exec();
        save_timer->start(30000);
        return false;
    }

    QString data("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
    data.append("<note version=\"0.3\" xmlns:link=\"http://beatniksoftware.com/tomboy/link\" xmlns:size=\"http://beatniksoftware.com/tomboy/size\" xmlns=\"http://beatniksoftware.com/tomboy\">");
    data.append("<title>" + noteTitles.at(currentNote) + "</title>");
    data.append("<text xml:space=\"preserve\"><note-content version=\"0.1\">");
    data.append("</note-content></text>");
    data.append("<last-change-date></last-change-date>");
    data.append("<last-metadata-change-date></last-metadata-change-date>");
    data.append("<create-date></create-date>");
    data.append("<cursor-position>0</cursor-position>");
    data.append("<width>0</width>");
    data.append("<height>0</height>");
    data.append("<x>-1</x>");
    data.append("<y>-1</y>");
    data.append("<open-on-startup>False</open-on-startup></note>");

    remote_control->SetNoteCompleteXml(noteUris.at(currentNote), data);
    saveNote();
    save_needed = false;
    return true;
}

void BloomWidgetNote::auto_save()
{
    saveCompleteXMLNote();
}

void BloomWidgetNote::delete_uri(QString uri)
{
    QStringList uris = notes->value().toStringList();
    uris.removeOne(uri);
    notes->set(uris, false);
}

void BloomWidgetNote::save_uri(QString newUri)
{
    QStringList uris = notes->value().toStringList();
    uris.append(newUri);
    notes->set(QVariant(uris), false);
}

void BloomWidgetNote::prevNote()
{
    if(currentNote > 0) {
        if(!saveCompleteXMLNote()) {
            return;
        }
        currentNote--;
        displayNote(currentNote);
    }
}

void BloomWidgetNote::nextNote()
{
    if(currentNote < notesCount-1) {
        if(!saveCompleteXMLNote()) {
            return;
        }
        currentNote++;
        displayNote(currentNote);
    }
}

void BloomWidgetNote::changeCharFormat(QTextCharFormat textFormat)
{
    if(textFormat.fontItalic()) {
        setItalicButton->setChecked(true);
    } else {
        setItalicButton->setChecked(false);
    }
    if(textFormat.fontStrikeOut()) {
        setStrikeButton->setChecked(true);
    } else {
        setStrikeButton->setChecked(false);
    }
    if(textFormat.fontWeight() == QFont::Bold) {
        setBoldButton->setChecked(true);
    } else {
        setBoldButton->setChecked(false);
    }
    
    qreal pointSize = textFormat.fontPointSize();
    if(pointSize == FONTSMALL) {
        smallSizeButton->setChecked(true);
    } else if(pointSize == FONTLARGE) {
        largeSizeButton->setChecked(true);
    } else if(pointSize == FONTHUGE) {
        hugeSizeButton->setChecked(true);
    } else {
        normalSizeButton->setChecked(true);
    }
}

void BloomWidgetNote::set_FontBold(bool active)
{
    if(active)
        noteText->setFontWeight(QFont::Bold);
    else
        noteText->setFontWeight(QFont::Normal);
}

void BloomWidgetNote::set_FontStrike(bool active)
{
    QTextCharFormat currentCharFormat = noteText->currentCharFormat();
    
    if(active)
        currentCharFormat.setFontStrikeOut(true);
    else
        currentCharFormat.setFontStrikeOut(false);
        
    noteText->setCurrentCharFormat(currentCharFormat);
}

void BloomWidgetNote::set_FontSize(QAction * fontSize)
{
    if(fontSize == smallSizeButton)
        noteText->setFontPointSize(FONTSMALL);
    else if(fontSize == normalSizeButton)
        noteText->setFontPointSize(FONTNORMAL);
    else if(fontSize == largeSizeButton)
        noteText->setFontPointSize(FONTLARGE);
    else if(fontSize == hugeSizeButton)
        noteText->setFontPointSize(FONTHUGE);
}
void BloomWidgetNote::activate_save()
{
    save_needed = true;
}

void BloomWidgetNote::activate_save(QString lineEditText)
{
    noteTitles.replace(currentNote, lineEditText);
    save_needed = true;
}

void BloomWidgetNote::resizeEvent(QResizeEvent *event)
{
    lineTitle->setCursorPosition(0);
    QWidget::resizeEvent(event);
}

