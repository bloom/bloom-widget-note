#!/bin/sh

_HELP=
_NAME=
_ISNAME=
_PARAMETRES=$@

if [ -z "$_PARAMETRES" ]
then
    echo " "
    echo "No parameter"
    _HELP=1
fi

for OPTION in $_PARAMETRES
do
    if [ $_ISNAME ]
    then
        if [ $OPTION != NULL ]
        then
            _NAME=$OPTION
        else
            _HELP=1
        fi
        _ISNAME=
    else
        case $OPTION in
        -n | --name)
            _ISNAME=1
            ;;
        *)
            echo " "
            echo "Parameter ERROR : $OPTION"
            _HELP=1
            ;;
        esac
    fi
done

if [[ $_ISNAME && ! $_NAME ]]
then
    echo " "
    echo "Parameter ERROR"
    _HELP=1
fi

if [ $_HELP ]
then
    echo " "
    echo "    [-n NAME]               new name for your widget"
    echo " "
    exit 0
else
    mv ./bloom-widget-note.spec ./bloom-widget-$_NAME.spec

    mv ./bloom-widget-note/bloom-widget-note.cpp ./bloom-widget-note/bloom-widget-$_NAME.cpp
    mv ./bloom-widget-note/bloom-widget-note.h ./bloom-widget-note/bloom-widget-$_NAME.h
    mv ./bloom-widget-note/bloom-widget-note.pc.in ./bloom-widget-note/bloom-widget-$_NAME.pc.in

    mv ./bloom-widget-note-action/bloom-widget-note-action.cpp ./bloom-widget-note-action/bloom-widget-$_NAME-action.cpp
    mv ./bloom-widget-note-action/bloom-widget-note-action.h ./bloom-widget-note-action/bloom-widget-$_NAME-action.h
    mv ./bloom-widget-note-action/bloom-widget-note-action.pc.in ./bloom-widget-note-action/bloom-widget-$_NAME-action.pc.in

    mv ./data/theme/bloom-widget-note.css ./data/theme/bloom-widget-$_NAME.css

    mv ./bloom-widget-note ./bloom-widget-$_NAME
    mv ./bloom-widget-note-action ./bloom-widget-$_NAME-action

    find ./ -type f | xargs sed -i 's/note/'$_NAME'/g'
    
    echo " "
    echo "End! The new name is bloom-widget-"$_NAME 
    echo " "
fi
