Name:           bloom-widget-note
Version:        0.1.0
Release:        1%{?dist}
Summary:        add %{name}

Group:          Development/Libraries
License:        Agorabox
URL:            http://www.agorabox.org/
Source0:        %{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: bloom-panel-home-devel
BuildRequires: dbus-glib-devel
BuildRequires: GConf2-devel
BuildRequires: gtk2-devel
BuildRequires: startup-notification-devel
BuildRequires: intltool
BuildRequires: qt-devel
BuildRequires: libtool
BuildRequires: gnome-common

 
Requires: redhat-menus tomboy

%description
add %{name} to bloom-panel-home

%prep
%setup -q


%build
./autogen.sh
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
%{_includedir}/bloom-widget-note-action/bloom-widget-note-action.h
%{_includedir}/bloom-widget-note/bloom-widget-note.h
%{_includedir}/bloom-widget-note/tomboy_wrapper.h
%{_libdir}/bloom-widgets/bloom-widget-note/libbloom-widget-note-action.a
%{_libdir}/bloom-widgets/bloom-widget-note/libbloom-widget-note-action.so*
%{_libdir}/bloom-widgets/bloom-widget-note/libbloom-widget-note.a
%{_libdir}/bloom-widgets/bloom-widget-note/libbloom-widget-note.so*
%{_libdir}/pkgconfig/bloom-widget-note-action.pc
%{_libdir}/pkgconfig/bloom-widget-note.pc
%{_datadir}/bloom-widget-note/


%changelog
* Wed Oct 27 2010 Julien Clement <julien.clement@agorabox.org> 0.1.0
- New 0.1.0 upstream release
